package org.example.eflObjects

import efl.*
import io.gitlab.embedSoft.eflKt.core.*
import io.gitlab.embedSoft.eflKt.core.event.EflEvent
import kotlinx.cinterop.*

private val globalArena = Arena()
private var root: EflObject? = null
private var child1: EflObject? = null
private var child2: EflObject? = null
private var rootRef = globalArena.alloc<CPointerVar<Eo>>()
private var child1Ref = globalArena.alloc<CPointerVar<Eo>>()
private var child2Ref = globalArena.alloc<CPointerVar<Eo>>()

fun main(args: Array<String>) {
    startCliApplication(args, ::eflMain)
}

private fun printStatus() {
    val tmpRoot = rootRef.value.toEflObject()
    val tmpChild1 = child1Ref.value.toEflObject()
    val tmpChild2 = child2Ref.value.toEflObject()
    println("Root Weak Ref Reference Count: ${tmpRoot.refCount}")
    println("Child1 Weak Ref Reference Count: ${tmpChild1.refCount}")
    println("Child2 Weak Ref Reference Count: ${tmpChild2.refCount}")
}

private fun eflMain(event: EflEvent) {
    event.fetchProgramArguments().forEachIndexed { pos, arg -> println("Arg $pos: $arg") }
    createObjects()
    println("Initial State:")
    printStatus()
    destroyObjects()
    println("Final State:")
    printStatus()
    exitApplication()
}

private fun destroyObjects() {
    // Destroy the root element.
    println("Deleting root...")
    root?.unReference()
    // Destroy the child2 element, for which we were keeping an extra reference.
    println("Deleting Child2...")
    child2?.unReference()
}

private fun createObjects() {
    // TODO: Find the class to use for root, and the class to use for child1/child2.
    // First create a root element.
    root = EflObject.createWithReference(EFL_GENERIC_MODEL_CLASS, null) { name = "Root" }
    // Add a weak reference so we can keep track of its state.
    root?.addWeakReference(rootRef.ptr)
    // Create the first child element. Note that Smart Casting won't work on rootObj.
    child1 = EflObject.create(EFL_GENERIC_MODEL_CLASS, root!!) { name = "Child1" }
    // Add a weak reference so we can keep track of its state.
    child1?.addWeakReference(child1Ref.ptr)
    // Create the second child element, this time, with an extra reference.
    child2 = EflObject.createWithReference(EFL_GENERIC_MODEL_CLASS, root) { name = "Child2" }
    // Add a weak reference so we can keep track of its state.
    child2?.addWeakReference(child2Ref.ptr)
}
