group = "org.example"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.5.31"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                val eflKtVer = "0.1"
                implementation("io.gitlab.embed-soft.efl-kt:eflkt-core:$eflKtVer")
            }
            cinterops.create("efl") {
                val baseIncludeDir = "/mnt/pi_image/usr/local/include"
                includeDirs(
                    "$baseIncludeDir/ecore-1",
                    "$baseIncludeDir/eina-1",
                    "$baseIncludeDir/eina-1/eina",
                    "$baseIncludeDir/eo-1",
                    "$baseIncludeDir/efl-1"
                )
            }
        }
        binaries {
            executable("efl_objects") {
                entryPoint = "org.example.eflObjects.main"
            }
        }
    }
}
